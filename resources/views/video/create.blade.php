@extends('layouts.sidenav')

@section('content')
    <div class="box">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Add New Video</h3>
            </div>

            <form method="post" action="{{ route('video.store') }}">
                @csrf
                <input type="hidden" name="usaha_id" value="{{ $usaha->id }}">
                <div class="box-body">
                    <div class="form-group">
                        <label for="name">Video Embeed Link</label>
                        <input type="text" class="form-control"  name="video" id="name" placeholder="video embeed link">
                    </div>
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
            </form>
        </div>
    </div>
@endsection
