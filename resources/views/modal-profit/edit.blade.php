@extends('layouts.sidenav')

@section('content')
    <div class="box">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Edit Modal & Profit</h3>
            </div>

            <form method="post" action="{{ route('modalprofit.update', $modalprofit) }}">
                @csrf
                @method('PUT')
                <input type="hidden" name="usaha_id" value="{{ $modalprofit->usaha_id }}">
                <div class="box-body">
                    <div class="form-group">
                        <label for="name">Modal</label> <p style="font-style: italic">*) masukkan tanpa Rp dan tanpa titik apabila angka ribuan</p>
                        <input type="number" value="{{ $modalprofit->modal }}" min="0" class="form-control"  name="modal" id="name" placeholder="modal">
                    </div>

                    <div class="form-group">
                        <label for="name">Profit</label> <p style="font-style: italic">*) masukkan tanpa Rp dan tanpa titik apabila angka ribuan</p>
                        <input type="number" min="0" value="{{ $modalprofit->profit }}" class="form-control"  name="profit" id="name" placeholder="profit">
                    </div>
                    <div class="form-group">
                        <label for="name">Presentasi Bagi Pendana</label> <p style="font-style: italic">*) masukkan tanpa % </p>
                        <input type="number" min="0" value="{{ $modalprofit->presentase_bagi_pendana }}" class="form-control"  name="presentase_bagi_pendana" id="name" placeholder="presentase bagi pendana">
                    </div>
                    <div class="form-group">
                        <label for="name">Jumlah Hari Persiapan</label> <p style="font-style: italic">*) masukkan tanpa hari</p>
                        <input type="number" min="0" class="form-control" value="{{ $modalprofit->jumlah_hari_persiapan }}" name="jumlah_hari_persiapan" id="name" placeholder="jumlah hari pesiapan">
                    </div>
                    <div class="form-group">
                        <label for="name">Periode Transaksi Per Hari</label> <p style="font-style: italic">*) masukkan tanpa hari</p>
                        <input type="number" min="0" class="form-control"  name="periode_transaksi_per_hari" value="{{ $modalprofit->periode_transaksi_per_hari }}" id="name" placeholder="periode transaksi per hari">
                    </div>
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
            </form>
        </div>
    </div>
@endsection
