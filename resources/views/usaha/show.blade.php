@extends('layouts.sidenav')

@section('content')
    <style>
        .switch {
            position: relative;
            display: inline-block;
            width: 60px;
            height: 34px;
        }

        .switch input {
            opacity: 0;
            width: 0;
            height: 0;
        }

        .slider {
            position: absolute;
            cursor: pointer;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background-color: #ccc;
            -webkit-transition: .4s;
            transition: .4s;
        }

        .slider:before {
            position: absolute;
            content: "";
            height: 26px;
            width: 26px;
            left: 4px;
            bottom: 4px;
            background-color: white;
            -webkit-transition: .4s;
            transition: .4s;
        }

        input:checked+.slider {
            background-color: #2196F3;
        }

        input:focus+.slider {
            box-shadow: 0 0 1px #2196F3;
        }

        input:checked+.slider:before {
            -webkit-transform: translateX(26px);
            -ms-transform: translateX(26px);
            transform: translateX(26px);
        }

        .slider.round {
            border-radius: 34px;
        }

        .slider.round:before {
            border-radius: 50%;
        }
    </style>


    @include('flash::message')
    <div class="box">
        <div class="box-header">
            <h3 class="box-title"></h3>
        </div>

        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12 col-md-12">
                    <div class="card">
                        <div class="card-header bg-light">
                            <strong>Detail Usaha</strong><br/>
                            <img style="margin-top: 20px;" width="80px" height="80px" src="{{ asset('images/'.$usaha->picture) }}">
                        </div>
                        <ul class="list-group list-group-flush" style="margin-top: 20px">
                            <li class="list-group-item">
                                <strong class="text-muted">Name</strong>
                                <span class="pull-right">{{ $usaha->name }}</span>
                            </li>
                            <li class="list-group-item">
                                <strong class="text-muted">Category</strong>
                                <span class="pull-right">{{ $usaha->category->name }}</span>
                            </li>
                            <li class="list-group-item">
                                <strong class="text-muted">Description</strong>
                                <span class="pull-right">{{ $usaha->description }}</span>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="box">
        <div class="box-header">
            <h3 class="box-title"></h3>
        </div>

        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12 col-md-12">
                    <div class="card">
                        <div class="card-header bg-light">
                            <strong>Alamat Usaha</strong>
                        </div>

                        <a href="{{ route('alamatusaha.create', $usaha->id) }}"  style="margin-left: 10px; margin-top: 20px;"  class="btn btn-sm btn-default"><span class="fa fa-plus"></span> Add New</a>

                         @if(!$alamatusahas->isEmpty())
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th class="text-center" style="width: 20px">No.</th>
                                    <th>{{ __('Jenis Alamat') }}</th>
                                    <th>{{ __('Alamat') }}</th>
                                    <th>{{ __('Active') }}</th>
                                    <th class="text-center pull-right">{{ __('Action') }}</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($alamatusahas as $i => $alamat)
                                    <tr>
                                        <td>{{ $i+1 }}</td>
                                        <td>
                                            <b>{{ $alamat->type }}</b>
                                        </td>
                                        <td>{{ $alamat->address }}</td>
                                        <td>
                                            <label class="switch">
                                                <input class="quickpick" data-status="{{$alamat->id}}" name="quick_pick" type="checkbox" @if($alamat->status == 1) checked @endif>
                                                <span class="slider round"></span>
                                            </label>
                                        </td>
                                        <td class="text-center pull-right" >
                                            <div class="btn-group">
                                                <a href="{{ route('alamatusaha.edit', $alamat) }}" class="btn btn-sm btn-warning">
                                                    <i class="fa fa-pencil"></i>
                                                </a>
                                                <a href="{{ route('alamatusaha.delete', $alamat) }}" class="btn btn-sm btn-danger"
                                                   onclick = "return confirm('Are you sure ?')">
                                                    <i class="fa fa-trash"></i>
                                                </a>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        @else
                            <div class="px-3 text-center" style="padding: 10px">
                                <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
                                    No Data
                                </p>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="box">
        <div class="box-header">
            <h3 class="box-title"></h3>
        </div>

        <div class="container-fluid" style="margin-top: 20px">
            <div class="row">
                <div class="col-sm-12 col-md-12">
                    <div class="card">
                        <div class="card-header bg-light">
                            <strong>Modal & Profit</strong><br/>
                        </div>

                        @if($modalProfits !== null )
                            <a href="{{ route('modalprofit.edit', $modalProfits->id) }}"  style="margin-left: 10px; margin-top: 20px;"  class="btn btn-sm btn-default"><span class="fa fa-edit"></span> Edit</a>
                        @else
                            <a href="{{ route('modalprofit.create', $usaha->id) }}"  style="margin-left: 10px; margin-top: 20px;"  class="btn btn-sm btn-default"><span class="fa fa-plus"></span> Add New</a>
                        @endif

                        @if($modalProfits !== null )
                            <ul class="list-group list-group-flush" style="margin-top: 20px">
                            <li class="list-group-item">
                                <strong class="text-muted">Modal</strong>
                                <span class="pull-right">{{ 'Rp'.number_format($modalProfits->modal,2,",",".")}}</span>
                            </li>
                            <li class="list-group-item">
                                <strong class="text-muted">Profit</strong>
                                <span class="pull-right">{{ 'Rp'.number_format($modalProfits->profit,2,",",".")}}</span>
                            </li>
                            <li class="list-group-item">
                                <strong class="text-muted">Presentase Bagi Pendana</strong>
                                <span class="pull-right">{{ $modalProfits->presentase_bagi_pendana.'%' }}</span>
                            </li>
                            <li class="list-group-item">
                                <strong class="text-muted">Jumlah Hari Persiapan</strong>
                                <span class="pull-right">{{ $modalProfits->jumlah_hari_persiapan.' Hari' }}</span>
                            </li>
                            <li class="list-group-item">
                                <strong class="text-muted">Periode Transaksi Per Hari</strong>
                                <span class="pull-right">{{ $modalProfits->periode_transaksi_per_hari.' Hari' }}</span>
                            </li>
                        </ul>
                        @else
                            <div class="px-3 text-center" style="padding: 10px">
                                <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
                                    No Data
                                </p>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="box">
        <div class="box-header">
            <h3 class="box-title"></h3>
        </div>

        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12 col-md-12">
                    <div class="card">
                        <div class="card-header bg-light">
                            <strong>Foto</strong>
                        </div>

                        <a href="{{ route('foto.create', $usaha->id) }}"  style="margin-left: 10px; margin-top: 20px;"  class="btn btn-sm btn-default"><span class="fa fa-plus"></span> Upload Foto</a>

                        @if(!$fotos->isEmpty())
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th class="text-center" style="width: 20px">No.</th>
                                    <th>{{ __('Foto') }}</th>
                                    <th>{{ __('Active') }}</th>
                                    <th class="text-center pull-right">{{ __('Action') }}</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($fotos as $i => $foto)
                                    <tr>
                                        <td>{{ $i+1 }}</td>
                                        <td>
                                            <img width="300px" height="100px" src="{{ $foto->image_url }}">
                                        </td>
                                        <td>
                                            <label class="switch">
                                                <input class="activetoggle" data-statustoggle="{{$foto->id}}" name="quick_pick" type="checkbox" @if($foto->status == 1) checked @endif>
                                                <span class="slider round"></span>
                                            </label>
                                        </td>
                                        <td class="text-center pull-right" >
                                            <div class="btn-group">
                                                <a  class="btn btn-sm btn-info" data-toggle="modal" data-target="#myModal{{$foto->id}}">
                                                    <i class="fa fa-eye"></i>
                                                </a>

                                                <div id="myModal{{ $foto->id }}" class="modal fade" role="dialog">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                                <h4 class="modal-title">Detail Foto</h4>
                                                            </div>
                                                            <div class="modal-body">
                                                                <center>
                                                                    <img width="500px" height="300px" src="{{ $foto->image_url }}">
                                                                </center>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                                <a href="{{ route('foto.edit', $foto) }}" class="btn btn-sm btn-warning">
                                                    <i class="fa fa-pencil"></i>
                                                </a>
                                                <a href="{{ route('foto.delete', $foto) }}" class="btn btn-sm btn-danger"
                                                   onclick = "return confirm('Are you sure ?')">
                                                    <i class="fa fa-trash"></i>
                                                </a>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        @else
                            <div class="px-3 text-center" style="padding: 10px">
                                <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
                                    No Data
                                </p>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="box">
        <div class="box-header">
            <h3 class="box-title"></h3>
        </div>

        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12 col-md-12">
                    <div class="card">
                        <div class="card-header bg-light">
                            <strong>Video</strong>
                        </div>

                        <a href="{{ route('video.create', $usaha->id) }}"  style="margin-left: 10px; margin-top: 20px;"  class="btn btn-sm btn-default"><span class="fa fa-plus"></span>Tambah Video</a>

                        @if(!$videos->isEmpty())
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th class="text-center" style="width: 20px">No.</th>
                                    <th>{{ __('Video') }}</th>
                                    <th>{{ __('Active') }}</th>
                                    <th class="text-center pull-right">{{ __('Action') }}</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($videos as $i => $video)
                                    <tr>
                                        <td>{{ $i+1 }}</td>
                                        <td>
                                            <iframe width="300" height="150" src="{{ $video->video }}" frameborder="0" allow="accelerometer; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                        </td>
                                        <td>
                                            <label class="switch">
                                                <input class="quickpickvideo" data-statusvideo="{{$video->id}}" name="quick_pick" type="checkbox" @if($video->status == 1) checked @endif>
                                                <span class="slider round"></span>
                                            </label>
                                        </td>
                                        <td class="text-center pull-right" >
                                            <div class="btn-group">
                                                <a  class="btn btn-sm btn-info" data-toggle="modal" data-target="#myModalVideo{{$video->id}}">
                                                    <i class="fa fa-eye"></i>
                                                </a>

                                                <div id="myModalVideo{{ $video->id }}" class="modal fade" role="dialog">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                                <h4 class="modal-title">Detail Video</h4>
                                                            </div>
                                                            <div class="modal-body">
                                                                <center>
                                                                    <iframe width="530" height="300" src="{{ $video->video }}" frameborder="0" allow="accelerometer;autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                                                </center>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                                <a href="{{ route('video.edit', $video) }}" class="btn btn-sm btn-warning">
                                                    <i class="fa fa-pencil"></i>
                                                </a>
                                                <a href="{{ route('video.delete', $video) }}" class="btn btn-sm btn-danger"
                                                   onclick = "return confirm('Are you sure ?')">
                                                    <i class="fa fa-trash"></i>
                                                </a>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        @else
                            <div class="px-3 text-center" style="padding: 10px">
                                <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
                                    No Data
                                </p>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="box">
        <div class="box-header">
            <h3 class="box-title"></h3>
        </div>

        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12 col-md-12">
                    <div class="card">
                        <div class="card-header bg-light">
                            <strong>Review</strong>
                        </div>

                        @if(!$reviews->isEmpty())
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th class="text-center" style="width: 20px">No.</th>
                                    <th>{{ __('User') }}</th>
                                    <th>{{ __('Review') }}</th>
                                    <th>{{ __('Active') }}</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($reviews as $i => $review)
                                    <tr>
                                        <td>{{ $i+1 }}</td>
                                        <td>

                                            {{ $review->user->name }}
                                        </td>
                                        <td>{{ $review->review  }}</td>
                                        <td>
                                            <label class="switch">
                                                <input class="quickpickreview" data-statusreview="{{$review->id}}" name="quick_pick" type="checkbox" @if($review->status == 1) checked @endif>
                                                <span class="slider round"></span>
                                            </label>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        @else
                            <div class="px-3 text-center" style="padding: 10px">
                                <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
                                    No Data
                                </p>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script type="text/javascript">
        $('.quickpick').on('click', function () {

            let id = $(this).data('status')
            let url = $(this).prop('checked')
                ? "{{ url('alamatusaha/update/status/1/') }}" + '/' + id
                : "{{ url('alamatusaha/update/status/0/') }}" + '/' + id

            $.ajax({
                url: url,
                success: function (response) {
                }
            })
        })

        $('.activetoggle').on('click', function () {

            let id = $(this).data('statustoggle')
            let url = $(this).prop('checked')
                ? "{{ url('foto/update/status/1/') }}" + '/' + id
                : "{{ url('foto/update/status/0/') }}" + '/' + id

            $.ajax({
                url: url,
                success: function (response) {
                }
            })
        })

        $('.quickpickvideo').on('click', function () {

            let id = $(this).data('statusvideo')
            let url = $(this).prop('checked')
                ? "{{ url('video/update/status/1/') }}" + '/' + id
                : "{{ url('video/update/status/0/') }}" + '/' + id

            $.ajax({
                url: url,
                success: function (response) {
                }
            })
        })

        $('.quickpickreview').on('click', function () {

            let id = $(this).data('statusreview')
            console.log(id)
            let url = $(this).prop('checked')
                ? "{{ url('review/update/status/1/') }}" + '/' + id
                : "{{ url('review/update/status/0/') }}" + '/' + id

            $.ajax({
                url: url,
                success: function (response) {
                }
            })
        })

    </script>
@endpush

