@extends('layouts.sidenav')

@section('content')
    <div class="box">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Edit Usaha</h3>
            </div>

            <form method="post" action="{{ route('usaha.update', $usaha) }}" enctype="multipart/form-data" accept-charset="UTF-8">
                @csrf
                @method('put')

                <div class="box-body">
                    <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" class="form-control"  value="{{ $usaha->name }}" name="name" id="name" placeholder="name">
                    </div>
                    <div class="form-group">
                        <label for="picture">Picture</label>
                        <input type="file" name="picture">
                    </div>
                    <div class="form-group">
                        <label>Select Category</label>
                        <select class="form-control" name="category_id">
                            @foreach($categories as $i => $category)
                                <option @if($category->id === $usaha->category_id) selected @endif  value="{{ $category->id }}">{{ $category->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="name">Description</label>
                        <textarea class="form-control" name="description">{{ $usaha->description }}</textarea>
                    </div>
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
            </form>
        </div>
    </div>
@endsection
