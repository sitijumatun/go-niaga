@extends('layouts.sidenav')

@section('content')
    <div class="box">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Add New Users</h3>
            </div>

            <form method="post" action="{{ route('user.store') }}" enctype="multipart/form-data" accept-charset="UTF-8">
                @csrf
                <div class="box-body">
                    <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" class="form-control"  name="name" id="name" placeholder="name" required>
                    </div>
                    <div class="form-group">
                        <label for="name">Email</label>
                        <input type="email" class="form-control"  name="email" id="email" placeholder="email" required>
                    </div>
                    <div class="form-group">
                        <label for="name">Password</label>
                        <input type="password" class="form-control"  name="password" id="password" placeholder="password" required>
                    </div>
                    <div class="form-group">
                        <label for="picture">Picture</label>
                        <input type="file" name="image">
                    </div>
                    <div class="form-group">
                        <label>Select Address Type</label>
                        <select class="form-control" name="address_type">
                            <option value=" ">No Data</option>
                            @foreach($addresstypes as $key => $addresstype)
                                <option value="{{ $key }}">{{ $addresstype }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Role</label>
                        <select class="form-control" name="role">
                            @foreach($roles as $key => $role)
                                <option value="{{ $key }}">{{ $role }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="name">Address</label>
                        <textarea class="form-control" name="address" placeholder="address" ></textarea>
                    </div>
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
            </form>
        </div>
    </div>
@endsection
