@extends('layouts.sidenav')

@section('content')
    <div class="box">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Edit Categories</h3>
            </div>

            <form method="post" action="{{ route('category.update', $category) }}">
                @csrf
                @method('put')

                <div class="box-body">
                    <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" class="form-control"  value="{{ $category->name }}" name="name" id="name" placeholder="name">
                    </div>
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
            </form>
        </div>
    </div>
@endsection
