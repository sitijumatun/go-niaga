@extends('layouts.sidenav')

@section('content')
    <style>
        .switch {
            position: relative;
            display: inline-block;
            width: 60px;
            height: 34px;
        }

        .switch input {
            opacity: 0;
            width: 0;
            height: 0;
        }

        .slider {
            position: absolute;
            cursor: pointer;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background-color: #ccc;
            -webkit-transition: .4s;
            transition: .4s;
        }

        .slider:before {
            position: absolute;
            content: "";
            height: 26px;
            width: 26px;
            left: 4px;
            bottom: 4px;
            background-color: white;
            -webkit-transition: .4s;
            transition: .4s;
        }

        input:checked+.slider {
            background-color: #2196F3;
        }

        input:focus+.slider {
            box-shadow: 0 0 1px #2196F3;
        }

        input:checked+.slider:before {
            -webkit-transform: translateX(26px);
            -ms-transform: translateX(26px);
            transform: translateX(26px);
        }

        .slider.round {
            border-radius: 34px;
        }

        .slider.round:before {
            border-radius: 50%;
        }
    </style>


    @include('flash::message')
    <div class="box">
        <div class="box-header">
            <h3 class="box-title"></h3>
        </div>

        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12 col-md-12">
                    <div class="card">
                        <div class="card-header bg-light">
                            <strong>Detail Berita</strong><br/>
                            <img style="margin-top: 20px;" width="400px" height="400px" src="{{ $berita->image_url }}">
                        </div>
                        <ul class="list-group list-group-flush" style="margin-top: 20px">
                            <li class="list-group-item">
                                <strong class="text-muted">Title</strong>
                                <span class="pull-right">{{ $berita->title }}</span>
                            </li>
                            <li class="list-group-item">
                                <strong class="text-muted">Created By</strong>
                                <span class="pull-right">{{ $berita->user->name }}</span>
                            </li>
                            <li class="list-group-item">
                                <strong class="text-muted">Created At</strong>
                                <span class="pull-right">{{ $berita->created_at }}</span>
                            </li>
                            <li class="list-group-item">
                                <strong class="text-muted">Content</strong>
                                </br>
                                <span>{{ $berita->berita }}</span>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection


