@extends('layouts.sidenav')

@section('content')
    <div class="box">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Edit Berita</h3>
            </div>

            <form method="post" action="{{ route('berita.update', $berita) }}" enctype="multipart/form-data" accept-charset="UTF-8">
                @csrf
                @method('PUT')
                <div class="box-body">
                    <div class="form-group">
                        <label for="name">Title</label>
                        <input type="text" class="form-control" value="{{ $berita->title }}"  name="title" id="name" placeholder="judul">
                    </div>
                    <div class="form-group">
                        <label for="picture">Picture</label>
                        <input type="file" name="picture">
                    </div>
                    <div class="form-group">
                        <label for="name">Content</label>
                        <textarea class="form-control" name="berita">{{ $berita->berita }}</textarea>
                    </div>
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
            </form>
        </div>
    </div>
@endsection
