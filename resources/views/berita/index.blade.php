@extends('layouts.sidenav')

@section('content')
    <style>
        .switch {
            position: relative;
            display: inline-block;
            width: 60px;
            height: 34px;
        }

        .switch input {
            opacity: 0;
            width: 0;
            height: 0;
        }

        .slider {
            position: absolute;
            cursor: pointer;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background-color: #ccc;
            -webkit-transition: .4s;
            transition: .4s;
        }

        .slider:before {
            position: absolute;
            content: "";
            height: 26px;
            width: 26px;
            left: 4px;
            bottom: 4px;
            background-color: white;
            -webkit-transition: .4s;
            transition: .4s;
        }

        input:checked+.slider {
            background-color: #2196F3;
        }

        input:focus+.slider {
            box-shadow: 0 0 1px #2196F3;
        }

        input:checked+.slider:before {
            -webkit-transform: translateX(26px);
            -ms-transform: translateX(26px);
            transform: translateX(26px);
        }

        .slider.round {
            border-radius: 34px;
        }

        .slider.round:before {
            border-radius: 50%;
        }
    </style>

    @include('flash::message')
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Berita</h3>
            <div class="box-tools">
                <form class="row">
                    <div class="input-group input-group-sm hidden-xs" style="width: 150px;">
                        <input class="form-control" placeholder="{{ __('Search') }}&hellip;" name="q" value="{{ request('q') }}">
                        <div class="input-group-btn">
                            <button class="btn btn-secondary"><i class="fa fa-search"></i></button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <div class="box-body no-padding">
            <a href="{{ route('berita.create') }}"  style="margin-left: 10px"  class="btn btn-sm btn-default"><span class="fa fa-plus"></span> Add New</a>

            @if(!$beritas->isEmpty())
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th class="text-center" style="width: 20px">No.</th>
                        <th>{{ __('TItle') }}</th>
                        <th>{{ __('Image Title') }}</th>
                        <th>{{ __('Content') }}</th>
                        <th>{{ __('Created By') }}</th>
                        <th>{{ __('Active') }}</th>
                        <th class="text-center pull-right">{{ __('Action') }}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($beritas as $i => $berita)
                        <tr>
                            <td>{{ $i+1 }}</td>
                            <td>
                                <img width="80px" height="80px" src="{{ $berita->image_url  }}">
                            </td>
                            <td>{{ $berita->title }}
                                <div class="text-muted small">
                                    {{ $berita->slug }}
                                </div>
                            </td>
                            <td>{{ substr($berita->berita, 0, 30) .'...' }}</td>
                            <td>{{ $berita->user->name }}</td>
                            <td>
                                <label class="switch">
                                    <input class="quickpick" data-status="{{$berita->id}}" name="quick_pick" type="checkbox" @if($berita->status == 1) checked @endif>
                                    <span class="slider round"></span>
                                </label>
                            </td>
                            <td class="text-center pull-right" >
                                <div class="btn-group">
                                    <a href="{{ route('berita.show',$berita) }}" class="btn btn-sm btn-info">
                                        <i class="fa fa-info"></i>
                                    </a>
                                    <a href="{{ route('berita.edit', $berita) }}" class="btn btn-sm btn-warning">
                                        <i class="fa fa-pencil"></i>
                                    </a>
                                    <a href="{{ route('berita.delete', $berita) }}" class="btn btn-sm btn-danger"
                                       onclick = "return confirm('Are you sure ?')">
                                        <i class="fa fa-trash"></i>
                                    </a>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <center>{{ $beritas->links() }}</center>
            @else
                <div class="px-3 text-center" style="padding: 10px">
                    <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
                        No Data
                    </p>
                </div>
            @endif
        </div>
    </div>
@endsection

@push('scripts')
    <script type="text/javascript">
        $('.quickpick').on('click', function () {

            let id = $(this).data('status')
            let url = $(this).prop('checked')
                ? "{{ url('berita/update/status/1/') }}" + '/' + id
                : "{{ url('berita/update/status/0/') }}" + '/' + id

            $.ajax({
                url: url,
                success: function (response) {
                }
            })
        })
    </script>
@endpush
