@extends('layouts.sidenav')

@section('content')
    <div class="box">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Add New Alamat Usaha</h3>
            </div>

            <form method="post" action="{{ route('alamatusaha.store') }}">
                @csrf
                <input type="hidden" value="{{$usaha->id}}" name="usaha_id">
                <div class="box-body">
                    <div class="form-group">
                        <label>Select Address Type</label>
                        <select class="form-control" name="address_type">
                            @foreach(\App\Constants\AddressUsahaType::all() as $key => $alamatType)
                                <option value="{{ $key }}">{{ $alamatType }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="name">Address</label>
                        <textarea class="form-control" name="address"></textarea>
                    </div>
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
            </form>
        </div>
    </div>
@endsection
