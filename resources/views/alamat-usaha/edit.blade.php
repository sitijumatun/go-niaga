@extends('layouts.sidenav')

@section('content')
    <div class="box">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Edit Alamat Usaha</h3>
            </div>

            <form method="post" action="{{ route('alamatusaha.update', $alamatusaha) }}">
                @csrf
                @method('PUT')
                <input type="hidden" value="{{$alamatusaha->usaha_id}}" name="usaha_id">
                <div class="box-body">
                    <div class="form-group">
                        <label>Select Address Type</label>
                        <select class="form-control" name="address_type">
                            @foreach(\App\Constants\AddressUsahaType::all() as $key => $alamatType)
                                <option value="{{ $key }}" @if($key == $alamatusaha->address_type) selected @endif>{{ $alamatType }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="name">Address</label>
                        <textarea class="form-control" name="address">{{ $alamatusaha->address }}</textarea>
                    </div>
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
            </form>
        </div>
    </div>
@endsection
