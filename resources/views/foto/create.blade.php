@extends('layouts.sidenav')

@section('content')
    <div class="box">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Multiple Upload Photo</h3>
                <p>*) browse dari file manager kemudian upload beberapa foto</p>
            </div>

            <form method="post" action="{{ route('foto.store') }}" enctype="multipart/form-data" accept-charset="UTF-8">
                @csrf
                <input type="hidden" name="usaha_id" value="{{ $usaha->id }}">
                <div class="box-body">
                    <div class="form-group">
                        <label for="picture">Picture</label>
                        <input type="file" name="image[]" required multiple>
                    </div>
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
            </form>
        </div>
    </div>
@endsection
