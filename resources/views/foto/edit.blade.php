@extends('layouts.sidenav')

@section('content')
    <div class="box">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Edit Upload Photo</h3>
            </div>

            <form method="post" action="{{ route('foto.update', $foto) }}" enctype="multipart/form-data" accept-charset="UTF-8">
                @csrf
                @method('PUT')
                <input type="hidden" name="usaha_id" value="{{ $foto->usaha_id }}">
                <div class="box-body">
                    <div class="form-group">
                        <label for="picture">Picture</label>
                        <input type="file" name="image" required>
                    </div>
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
            </form>
        </div>
    </div>
@endsection
