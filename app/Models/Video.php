<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Video extends Model
{
    protected $fillable = [
      'video',
      'status'
    ];

    /**
     * @return BelongsTo
     */
    public function usaha(){
        return $this->belongsTo(Usaha::class);
    }
}
