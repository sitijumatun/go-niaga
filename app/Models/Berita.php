<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Berita extends Model
{
    protected $appends = ['image_url'];
    protected $fillable = [
          'title',
          'slug',
          'image',
          'berita',
          'status'
    ];

    /**
     * @return BelongsTo
     */
    public function user(){
        return $this->belongsTo(User::class);
    }


    /**
     * @return UrlGenerator|null|string
     */
    public function getImageUrlAttribute(){
        if(isset($this->attributes['image'])) {
            return url(asset('images/' . $this->attributes['image']));
        }

        return null;
    }
}
