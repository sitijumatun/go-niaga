<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Pemasukan extends Model
{
    protected $fillable = [
        'date',
        'nama_barang',
        'pemasukan'
    ];

    /**
     * @return BelongsTo
     */
    public function usaha(){
        return $this->belongsTo(Usaha::class, 'usaha_id');
    }
}
