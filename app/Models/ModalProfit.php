<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class ModalProfit extends Model
{
    protected $fillable = [
        'modal',
        'profit',
        'presentase_bagi_pendana',
        'jumlah_hari_persiapan',
        'periode_transaksi_per_hari'
    ];

    /**
     * @return BelongsTo
     */
    public function usaha(){
        return $this->belongsTo(Usaha::class);
    }
}
