<?php

namespace App\Models;

use App\Constants\AddressUsahaType;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class AlamatUsaha extends Model
{
    protected $appends = ['type'];
    protected $fillable = [
        'address_type',
        'address',
        'status'
    ];

    /**
     * @return BelongsTo
     */
    public function usaha(){
        return $this->belongsTo(Usaha::class);
    }

    /**
     * @return string
     * @throws \ReflectionException
     */
    public function getTypeAttribute(){
        return AddressUsahaType::getTitle($this->attributes['address_type']);
    }

    /**
     * @return mixed
     */
    public function active(){
        return $this->where('status', true);
    }
}
