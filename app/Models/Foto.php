<?php

namespace App\Models;

use Illuminate\Contracts\Routing\UrlGenerator;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Foto extends Model
{
    protected $appends = ['image_url'];
    protected $fillable = [
        'image',
        'status'
    ];

    /**
     * @return BelongsTo
     */
    public function usaha(){
        return $this->belongsTo(Usaha::class);
    }

    /**
     * @return UrlGenerator|null|string
     */
    public function getImageUrlAttribute(){
        if(isset($this->attributes['image'])) {
            return url(asset('images/' . $this->attributes['image']));
        }

        return null;
    }
}
