<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Pengeluaran extends Model
{
    protected $fillable = [
        'date',
        'nama_barang',
        'pengeluaran'
    ];

    /**
     * @return BelongsTo
     */
    public function usaha(){
        return $this->belongsTo(Usaha::class, 'usaha_id');
    }
}
