<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Usaha extends Model
{
    protected $appends = ['picture_url'];
    protected $fillable = array(
        'name',
        'description',
        'picture',
    );


    /**
     * @return BelongsTo
     */
    public function category(){
        return $this->belongsTo(Category::class);
    }

    /**
     * @return HasOne
     */
    public function modalprofit(){
        return $this->hasOne(ModalProfit::class);
    }

    /**
     * @return HasMany
     */
    public function fotos(){
        return $this->hasMany(Foto::class);
    }

    /**
     * @return HasMany
     */
    public function review(){
        return $this->hasMany(Review::class);
    }

    /**
     * @return HasMany
     */
    public function videos(){
        return $this->hasMany(Video::class);
    }

    /**
     * @return HasMany
     */
    public function alamatusaha(){
        return $this->hasMany(AlamatUsaha::class);
    }

    /**
     * @return BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return UrlGenerator|null|string
     */
    public function getPictureUrlAttribute(){
        if(isset($this->attributes['picture'])) {
            return url(asset('images/' . $this->attributes['picture']));
        }

        return null;
    }


}
