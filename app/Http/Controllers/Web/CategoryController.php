<?php

namespace App\Http\Controllers\Web;

use App\Models\Category;
use Exception;
use Illuminate\Contracts\View\Factory;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\ValidationException;
use Illuminate\View\View;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return Factory|View
     */
    public function index(Request $request)
    {
        $categories = Category::when($request->get('q'), static function (Builder $query) use ($request){
                        return $query->where('name', 'like','%'.$request->get('q').'%');
                    })->paginate();

        return view('category.index', compact('categories'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Factory|View
     */
    public function create()
    {
        return view('category.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return RedirectResponse
     * @throws ValidationException
     */
    public function store(Request $request)
    {
        $this->validate($request,[
           'name' => 'required'
        ]);

        $category = new Category();
        $category->fill($request->all());
        $category->save()
            ? flash('Data berhasil disimpan!')->success()
            : flash('Data gagal disimpan !')->error();

        return redirect()->route('category.index');
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param Category $category
     * @return Factory|View
     */
    public function edit(Category $category)
    {
        return view('category.edit', compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Category $category
     * @return RedirectResponse
     * @throws ValidationException
     */
    public function update(Request $request, Category $category)
    {
        $this->validate($request, [
            'name' => 'required'
        ]);

        $category->fill($request->all());
        $category->save()
            ? flash('Data berhasil diupdate!')->success()
            : flash('Data gagal diupdate!')->error();

        return redirect()->route('category.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Category $category
     * @return RedirectResponse
     * @throws Exception
     */
    public function destroy(Category $category)
    {
        $category->delete()
            ? flash('Data berhasil didelete!')->success()
            : flash('Data gagal diupdate!')->error();

        return redirect()->route('category.index');

    }


    /**
     * @param $status
     * @param Category $category
     */
    public function updateStatus($status, Category $category){
        $category->status = $status;
        $category->save();
    }
}
