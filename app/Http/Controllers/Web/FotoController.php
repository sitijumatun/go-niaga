<?php

namespace App\Http\Controllers\Web;

use App\Models\Foto;
use App\Models\Usaha;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\ValidationException;
use Illuminate\View\View;

class FotoController extends Controller
{
    /**
     * @param Usaha $usaha
     * @return Factory|View
     */
    public function create(Usaha $usaha){
        return view('foto.create', compact('usaha'));
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     * @throws ValidationException
     */
    public function store(Request $request){
        $this->validate($request, [
           'usaha_id' => 'required',
           'image' => 'required'
        ]);

        if(count($request->image) == 0){
            flash('Image tidak boleh null!')->error();
            return redirect()->back();
        }

        try{
            foreach ($request->image as $image) {
                $ext2 = $image->getClientOriginalExtension();
                $newName2 = random_int(100000, 1001238912) . '.' . $ext2;
                $image->move(public_path('images'), $newName2);
                $foto = new Foto();
                $foto->usaha()->associate($request->get('usaha_id'));
                $foto->image = $newName2;
                $foto->save();
            }

            flash('Data berhasil disimpan!');
            return redirect()->route('usaha.show', $request->get('usaha_id'));

        }catch (\Exception $e){
            flash($e->getMessage())->error();
            return redirect()->back();
        }
    }

    /**
     * @param Foto $foto
     * @return Factory|View
     */
    public function edit(Foto $foto){
        return view('foto.edit', compact('foto'));
    }

    /**
     * @param Request $request
     * @param Foto $foto
     * @return RedirectResponse
     * @throws \Exception
     */
    public function update(Request $request, Foto $foto){
        try {
            $this->validate($request, [
                'usaha_id' => 'required',
                'image' => 'required'
            ]);
        } catch (ValidationException $e) {
            flash('Image tidak boleh null!')->error();
            return redirect()->back();
        }

        $image = $request->image;
        $ext2 = $image->getClientOriginalExtension();
        $newName2 = random_int(100000, 1001238912) . '.' . $ext2;
        $image->move(public_path('images'), $newName2);

        $foto->usaha()->associate($request->get('usaha_id'));
        $foto->image = $newName2;
        $foto->update();

        flash('Data berhasil diupdate!');
        return redirect()->route('usaha.show', $request->get('usaha_id'));
    }

    /**
     * @param Foto $foto
     * @return RedirectResponse
     * @throws \Exception
     */
    public function destroy(Foto $foto){
        $foto->delete()
            ? flash('Data berhasil dihapus!')->success()
            : flash('Data gagal dihapus!')->error();

        return redirect()->back();
    }

    /**
     * @param $status
     * @param Foto $foto
     */
    public function updateStatus($status, Foto $foto){
        $foto->status = $status;
        $foto->save();
    }
}
