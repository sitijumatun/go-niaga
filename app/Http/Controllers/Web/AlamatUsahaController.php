<?php

namespace App\Http\Controllers\Web;

use App\Models\AlamatUsaha;
use App\Models\Usaha;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\ValidationException;
use Illuminate\View\View;

class AlamatUsahaController extends Controller
{
    /**
     * @param Usaha $usaha
     * @return Factory|View
     */
    public function create(Usaha $usaha){
        return view('alamat-usaha.create', compact('usaha'));
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function store(Request $request){
        try {
            $this->validate($request, [
                'address_type' => 'required',
                'usaha_id' => 'required',
                'address' => 'required'
            ]);
        } catch (ValidationException $e) {
            flash($e->getMessage())->error();
            return redirect()->back();
        }

        $alamatusaha = new AlamatUsaha();
        $alamatusaha->fill($request->all());
        $alamatusaha->usaha()->associate($request->get('usaha_id'));
        $alamatusaha->save()
            ? flash('Data berhasil disimpan!')->success()
            : flash('Data gagal disimpan')->error();

        return redirect()->route('usaha.show', $request->get('usaha_id'));
    }

    /**
     * @param $status
     * @param AlamatUsaha $alamatusaha
     */
    public function updateStatus($status, AlamatUsaha $alamatusaha){
        $alamatusaha->status = $status;
        $alamatusaha->save();
    }

    /**
     * @param AlamatUsaha $alamatusaha
     * @return RedirectResponse
     * @throws \Exception
     */
    public function destroy(AlamatUsaha $alamatusaha){
        $usaha_id = $alamatusaha->usaha_id;
        $alamatusaha->delete()
            ? flash('Data berhasil dihapus!')->success()
            : flash('Data gagal dihapus!')->error();

        return redirect()->route('usaha.show',$usaha_id);
    }

    /**
     * @param AlamatUsaha $alamatusaha
     * @return Factory|View
     */
    public function edit(AlamatUsaha $alamatusaha){
        return view('alamat-usaha.edit', compact('alamatusaha'));
    }

    /**
     * @param Request $request
     * @param AlamatUsaha $alamatusaha
     * @return RedirectResponse
     */
    public function update(Request $request, AlamatUsaha $alamatusaha){
        try {
            $this->validate($request, [
                'address_type' => 'required',
                'usaha_id' => 'required',
                'address' => 'required'
            ]);
        } catch (ValidationException $e) {
            flash($e->getMessage())->error();
            return redirect()->back();
        }

        $alamatusaha->fill($request->all());
        $alamatusaha->usaha()->associate($request->get('usaha_id'));
        $alamatusaha->update()
            ? flash('Data berhasil diupdate!')->success()
            : flash('Data gagal diupdate!')->error();

        return redirect()->route('usaha.show', $request->get('usaha_id'));
    }
}
