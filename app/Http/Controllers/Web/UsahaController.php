<?php

namespace App\Http\Controllers\Web;

use App\Models\AlamatUsaha;
use App\Models\Category;
use App\Models\Foto;
use App\Models\ModalProfit;
use App\Models\Review;
use App\Models\Usaha;
use App\Models\Video;
use Exception;
use Illuminate\Contracts\View\Factory;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Illuminate\Validation\ValidationException;
use Illuminate\View\View;

class UsahaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return Factory|View
     */
    public function index(Request $request)
    {
        $usahas = Usaha::when($request->get('q'), static function (Builder $query) use ($request){
            $query->where('name', 'like', '%'.$request->get('q').'%');
        })
            ->paginate();

        return view('usaha.index', compact('usahas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Factory|View
     */
    public function create()
    {
        $categories = Category::where('status', true)->get();

        return view('usaha.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return RedirectResponse
     * @throws ValidationException
     */
    public function store(Request $request)
    {
        $this->validate($request, [
           'name' => 'required',
           'description' => 'required',
           'category_id'=> 'required',
           'picture' => 'required'
        ]);

        $input = [
            'name' => $request->get('name'),
            'description' => $request->get('description'),
        ];

        if($request->has('picture')){
            $image = $request->picture;
            $ext2 = $image->getClientOriginalExtension();
            $newName2 = random_int(100000, 1001238912) . '.' . $ext2;
            $image->move(public_path('images'), $newName2);
            $input +=[
                'picture' => $newName2
            ];
        }

        $usaha = new Usaha();
        $usaha->fill($input);
        $usaha->category()->associate($request->get('category_id'));
        $usaha->user()->associate($request->user());

        $usaha->save()
            ? flash('Data berhasil disimpan!')->success()
            : flash('Data gagal disimpan!')->error();

        return redirect()->route('usaha.index');
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param Usaha $usaha
     * @return Factory|View
     */
    public function edit(Usaha $usaha)
    {
        $categories = Category::where('status', true)->get();

        return view('usaha.edit', compact('categories', 'usaha'));
    }

    /**
     * @param Usaha $usaha
     * @return Factory|View
     */
    public function show(Usaha $usaha){
        $alamatusahas = AlamatUsaha::where('usaha_id', $usaha->id)->get();
        $modalProfits = ModalProfit::where('usaha_id',$usaha->id)->first();
        $fotos = Foto::where('usaha_id', $usaha->id)->get();
        $videos = Video::where('usaha_id', $usaha->id)->get();
        $reviews = Review::where('usaha_id', $usaha->id)->get();

        return view('usaha.show', compact('usaha','alamatusahas', 'modalProfits', 'fotos', 'videos', 'reviews'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Usaha $usaha
     * @return RedirectResponse
     * @throws ValidationException
     */
    public function update(Request $request, Usaha $usaha)
    {
        $this->validate($request, [
            'name' => 'required',
            'description' => 'required',
            'category_id'=> 'required'
        ]);

        $input = [
            'name' => $request->get('name'),
            'description' => $request->get('description'),
        ];

        if($request->has('picture')){
            $image = $request->picture;
            $ext2 = $image->getClientOriginalExtension();
            $newName2 = random_int(100000, 1001238912) . '.' . $ext2;
            $image->move(public_path('images'), $newName2);
            $input +=[
                'picture' => $newName2
            ];
        }

        $usaha->fill($input);
        $usaha->category()->associate($request->get('category_id'));
        $usaha->user()->associate($request->user());


        $usaha->save()
            ? flash('Data berhasil diupdate!')->success()
            : flash('Data gagal diupdate!')->error();

        return redirect()->route('usaha.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Usaha $usaha
     * @return RedirectResponse
     * @throws Exception
     */
    public function destroy(Usaha $usaha)
    {
        $usaha->delete()
            ? flash('Data berhasil didelete!')->success()
            : flash('Data gagal didelete!')->error();

        return redirect()->route('usaha.index');
    }

    /**
     * @param $status
     * @param Usaha $usaha
     */
    public function updateStatus($status, Usaha $usaha){
        $usaha->status = $status;
        $usaha->save();
    }
}
