<?php

namespace App\Http\Controllers\Web;

use App\Models\Usaha;
use App\Models\Video;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\ValidationException;
use Illuminate\View\View;

class VideoController extends Controller
{
    /**
     * @param Usaha $usaha
     * @return Factory|View
     */
    public function create(Usaha $usaha){
        return view('video.create', compact('usaha'));
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function store(Request $request){
        try {
            $this->validate($request, [
                'usaha_id' => 'required',
                'video' => 'required'
            ]);
        } catch (ValidationException $e) {
            flash($e->getMessage());
            return redirect()->back();
        }

        $video = new Video();
        $video->fill($request->all());
        $video->usaha()->associate($request->get('usaha_id'));
        $video->save()
            ? flash('Data video berhasil disimpan!')->success()
            : flash('Data video gagal disimpan!')->error();

        return redirect()->route('usaha.show', $request->get('usaha_id'));
    }

    /**
     * @param $status
     * @param Video $video
     */
    public function updateStatus($status, Video $video){
        $video->status = $status;
        $video->save();
    }

    /**
     * @param Video $video
     * @return Factory|View
     */
    public function edit(Video $video){
        return view('video.edit', compact('video'));
    }

    /**
     * @param Request $request
     * @param Video $video
     * @return RedirectResponse
     */
    public function update(Request $request, Video $video){
        try {
            $this->validate($request, [
                'usaha_id' => 'required',
                'video' => 'required'
            ]);
        } catch (ValidationException $e) {
            flash($e->getMessage());
            return redirect()->back();
        }

        $video->fill($request->all());
        $video->usaha()->associate($request->get('usaha_id'));
        $video->update()
            ? flash('Data video berhasil diubah!')->success()
            : flash('Data video gagal diubah!')->error();

        return redirect()->route('usaha.show', $request->get('usaha_id'));
    }

    /**
     * @param Video $video
     * @return RedirectResponse
     * @throws \Exception
     */
    public function destroy(Video $video){
        $video->delete()
            ? flash('Data berhasil dihapus')->success()
            : flash('Data gagal dihapus')->error();

        return redirect()->back();
    }
}
