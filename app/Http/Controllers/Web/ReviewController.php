<?php

namespace App\Http\Controllers\Web;

use App\Models\Review;
use App\Http\Controllers\Controller;

class ReviewController extends Controller
{
    /**
     * @param $status
     * @param Review $review
     */
    public function updateStatus($status, Review $review){
        $review->status = $status;
        $review->save();
    }
}
