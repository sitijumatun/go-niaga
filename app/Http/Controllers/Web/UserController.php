<?php

namespace App\Http\Controllers\Web;

use App\Constants\AddressType;
use App\Constants\Role;
use App\Models\User;
use Illuminate\Contracts\View\Factory;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\ValidationException;
use Illuminate\View\View;

class UserController extends Controller
{
    /**
     * @param Request $request
     * @return Factory|View
     */
    public function index(Request $request){
        $users = User::when($request->has('q'), static function (Builder $query) use ($request){
            return $query->where('name', 'like', '%'.$request->get('q').'%')
                        ->orwhere('email', 'like', '%'.$request->get('q').'%')
                        ->orwhere('role', 'like', '%'.$request->get('q').'%');
        })
            ->orderBy('name')
            ->paginate();

        return view('users.index', compact('users'));
    }

    /**
     * @return Factory|View
     * @throws \ReflectionException
     */
    public function create(){
        $addresstypes = AddressType::all();
        $roles = Role::all();
        return view('users.create', compact('roles', 'addresstypes'));
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     * @throws ValidationException
     */
    public function store(Request $request){
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required',
            'password' => 'required',
            'role' => 'required'
        ]);

        $input = $request->all();
        if($request->has('image')){
            $image = $request->image;
            $ext2 = $image->getClientOriginalExtension();
            $newName2 = random_int(100000, 1001238912) . '.' . $ext2;
            $image->move(public_path('images'), $newName2);
            $input +=[
                'picture' => $newName2
            ];
        }

        $user = new User();
        $user->password = bcrypt($request->get('password'));
        $user->fill($input);
        $user->save()
            ? flash('Data created')->success()
            : flash('Data failed to create')->error();

        return redirect()->route('user.index');
    }

    /**
     * @param User $user
     * @return Factory|View
     * @throws \ReflectionException
     */
    public function edit(User $user){
        $addresstypes = AddressType::all();
        $roles = Role::all();
        return view('users.edit', compact('user', 'roles', 'addresstypes'));
    }

    /**
     * @param Request $request
     * @param User $user
     * @return RedirectResponse
     * @throws ValidationException
     */
    public function update(Request $request, User $user){
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required',
            'role' => 'required',
        ]);

        if($request->has('image')){
            $image = $request->image;
            $ext2 = $image->getClientOriginalExtension();
            $newName2 = random_int(100000, 1001238912) . '.' . $ext2;
            $image->move(public_path('images'), $newName2);
            $user->picture = $newName2;
        }
        if($request->has('password')) {
            $user->password = bcrypt($request->get('password'));
        }

        $user->name = $request->get('name');
        $user->email = $request->get('email');
        $user->address = $request->get('address');
        $user->address_type = $request->get('address_type');
        $user->role = $request->get('role');
        $user->update()
            ? flash('Data updated')->success()
            : flash('Data failed to update')->error();

        return redirect()->route('user.index');
    }

    /**
     * @param User $user
     * @return RedirectResponse
     * @throws \Exception
     */
    public function destroy(User $user){
        $user->delete()
            ? flash('Data deleted')->success()
            : flash('Data failed to delete')->error();

        return redirect()->route('user.index');
    }

    /**
     * @param $status
     * @param User $user
     */
    public function updateStatus($status, User $user){
        $user->is_active = $status;
        $user->save();
    }
}
