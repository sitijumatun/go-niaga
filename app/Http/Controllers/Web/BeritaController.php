<?php

namespace App\Http\Controllers\Web;

use App\Models\Berita;
use Illuminate\Contracts\View\Factory;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\ValidationException;
use Illuminate\View\View;

class BeritaController extends Controller
{
    /**
     * @param Request $request
     * @return Factory|View
     */
    public function index(Request $request){
        $beritas = Berita::orderBy('created_at','desc')
                ->when($request->has('q'), static function(Builder $query) use ($request){
                    return $query->where('title', 'like', '%'.$request->get('q').'%');
                })
                ->paginate();
        return view('berita.index', compact('beritas'));
    }

    /**
     * @return Factory|View
     */
    public function create(){
        return view('berita.create');
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     * @throws \Exception
     */
    public function store(Request $request){
        try {
            $this->validate($request, [
                'title' => 'required',
                'picture' => 'required',
                'berita' => 'required'
            ]);
        } catch (ValidationException $e) {
            flash($e->getMessage());
            return redirect()->back();
        }

        $input = $request->all();
        $slug = str_replace(' ', '-', $request->get('title'));
        $input +=[
            'slug' => $slug
        ];

        if($request->has('picture')){
            $image = $request->picture;
            $ext2 = $image->getClientOriginalExtension();
            $newName2 = random_int(100000, 1001238912) . '.' . $ext2;
            $image->move(public_path('images'), $newName2);
            $input +=[
                'image' => $newName2
            ];
        }

        $berita = new Berita();
        $berita->fill($input);
        $berita->user()->associate($request->user());
        $berita->save()
            ? flash('Data berhasil disimpan!')->success()
            : flash('Data gagal disimpan!')->error();

        return redirect()->route('berita.index');
    }

    /**
     * @param $id
     * @return Factory|View
     */
    public function edit($id){
        $berita = Berita::find($id);
        return view('berita.edit', compact('berita'));
    }

    /**
     * @param Request $request
     * @param $id
     * @return RedirectResponse
     * @throws \Exception
     */
    public function update(Request $request, $id){
        try {
            $this->validate($request, [
                'title' => 'required',
                'berita' => 'required'
            ]);
        } catch (ValidationException $e) {
            flash($e->getMessage());
            return redirect()->back();
        }

        $berita = Berita::find($id);

        $input = $request->all();
        $slug = str_replace(' ', '-', $request->get('title'));
        $input +=[
          'slug' => $slug
        ];

        if($request->has('picture')){
            $image = $request->picture;
            $ext2 = $image->getClientOriginalExtension();
            $newName2 = random_int(100000, 1001238912) . '.' . $ext2;
            $image->move(public_path('images'), $newName2);
            $input +=[
                'image' => $newName2
            ];
        }

        $berita->fill($input);
        $berita->user()->associate($request->user());
        $berita->update()
            ? flash('Data berhasil diubah!')->success()
            : flash('Data gagal diubah!')->error();

        return redirect()->route('berita.index');
    }

    /**
     * @param Berita $berita
     * @return RedirectResponse
     * @throws \Exception
     */
    public function destroy(Berita $berita){
        $berita->delete()
            ? flash('Data berhasil dihapus!')->success()
            : flash('Data gagal disimpan!')->error();

        return redirect()->route('berita.index');
    }

    /**
     * @param $id
     * @return Factory|View
     */
    public function show($id){
        $berita = Berita::find($id);
        return view('berita.show', compact('berita'));
    }

    /**
     * @param $status
     * @param Berita $berita
     */
    public function updateStatus($status, Berita $berita){
        $berita->status = $status;
        $berita->save();
    }
}
