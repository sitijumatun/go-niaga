<?php

namespace App\Http\Controllers\Web;

use App\Models\ModalProfit;
use App\Models\Usaha;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\ValidationException;
use Illuminate\View\View;

class ModalProfitController extends Controller
{
    /**
     * @param Usaha $usaha
     * @return Factory|View
     */
    public function create(Usaha $usaha){
        return view('modal-profit.create', compact('usaha'));
    }

    /**
     * @param Request $request
     * @return Factory|View
     * @throws ValidationException
     */
    public function store(Request $request){
        $this->validate($request,[
            'modal' => 'required',
            'profit' => 'required',
            'usaha_id' => 'required',
            'presentase_bagi_pendana' => 'required',
            'jumlah_hari_persiapan' => 'required',
            'periode_transaksi_per_hari' => 'required'
        ]);

        $modalProfit = new ModalProfit();
        $modalProfit->fill($request->all());
        $modalProfit->usaha()->associate($request->get('usaha_id'));
        $modalProfit->save()
            ? flash('Data berhasil disimpan!')->success()
            : flash('Data gagal disimpan!')->error();

        return redirect()->route('usaha.show', $request->get('usaha_id'));
    }

    /**
     * @param ModalProfit $modalprofit
     * @return Factory|View
     */
    public function edit(ModalProfit $modalprofit){
        return view('modal-profit.edit', compact('modalprofit'));
    }

    /**
     * @param Request $request
     * @param ModalProfit $modalProfit
     * @return RedirectResponse
     * @throws ValidationException
     */
    public function update(Request $request, ModalProfit $modalprofit){
        $this->validate($request,[
            'modal' => 'required',
            'profit' => 'required',
            'usaha_id' => 'required',
            'presentase_bagi_pendana' => 'required',
            'jumlah_hari_persiapan' => 'required',
            'periode_transaksi_per_hari' => 'required'
        ]);

        $modalprofit->fill($request->all());
        $modalprofit->usaha()->associate($request->get('usaha_id'));
        $modalprofit->update()
            ? flash('Data berhasil diupdate!')->success()
            : flash('Data gagal diupdate!')->error();

        return redirect()->route('usaha.show', $request->get('usaha_id'));
    }


}
