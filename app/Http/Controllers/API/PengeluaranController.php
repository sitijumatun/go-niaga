<?php

namespace App\Http\Controllers\API;

use App\Exceptions\APIException;
use App\Models\Pengeluaran;
use Illuminate\Http\Request;
use App\Models\Usaha;
use App\Http\Controllers\Controller;
use Illuminate\Validation\ValidationException;

class PengeluaranController extends Controller
{
    /**
     * @param Request $request
     * @return JsonResponse
     * @throws APIException
     * @throws ValidationException
     */
    public function store(Request $request){
        $this->validate($request, [
            'date' => 'required',
            'usaha_id' => 'required',
            'pengeluaran.*.nama_barang' => 'required',
            'pengeluaran.*.pengeluaran' => 'required'
        ]);

        $usaha = Usaha::find($request->get('usaha_id'));
        if(!$usaha){
            throw new APIException('UMKM tidak ditemukan!');
        }

        try{
            $resultData = array();
            foreach ($request->get('pengeluaran') as $pengeluaranData) {
                $input = [
                    'date' => $request->get('date'),
                    'usaha_id' => $request->get('usaha_id'),
                    'nama_barang' => $pengeluaranData['nama_barang'],
                    'pengeluaran' => $pengeluaranData['pengeluaran']
                ];

                $pengeluaran = new Pengeluaran();
                $pengeluaran->fill($input);
                $pengeluaran->usaha()->associate($usaha);
                $pengeluaran->save();

                array_push($resultData, $pengeluaran);
            }
        }catch (\Exception $e){
            throw new APIException($e->getMessage());
        }

        return response()->json([
            'status'   => 'success',
            'message'  => 'Input pengeluaran success!',
            'code'     => 200,
            'payload'  => $resultData
        ]);
    }
}
