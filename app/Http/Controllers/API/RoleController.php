<?php

namespace App\Http\Controllers\API;

use App\Constants\Role;
use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;

class RoleController extends Controller
{
    /**
     * @return JsonResponse
     * @throws \ReflectionException
     */
    public function index(){
        $result = array();
        foreach (Role::all() as $key => $role){
            $role = [
                $key => $role
            ];

            array_push($result, $role);
        }

        return response()->json([
            'status'   => 'success',
            'message'  => 'role retrieved!',
            'code'     => 200,
            'payload'  => $result
        ]);
    }
}
