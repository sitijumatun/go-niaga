<?php

namespace App\Http\Controllers\API;

use App\Constants\Role;
use App\Models\Category;
use App\Models\Usaha;
use App\Transformers\UsahaTransformer;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use App\Transformers\CategoryTransformer;

use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;

class CategoryController extends Controller
{
    /**
     * @return JsonResponse
     */
    public function index()
    {
        $categories = Category::where('status', true)->get();

        return response()->json([
            'status'   => 'success',
            'message'  => 'category retrieved',
            'code'     => 200,
            'payload'  => fractal($categories, new CategoryTransformer())
        ]);
    }

    /**
     * @param Request $request
     * @param Category $category
     * @return JsonResponse
     */
    public function listUMKM(Request $request, Category $category){
        $usahas = Usaha::where('category_id', $category->id)
                ->when($request->user()->role == Role::PEMILIK_USAHA || $request->user()->role == Role::OPERATOR_USAHA, static function (Builder $query) use ($request){
                    return $query->where('user_id', $request->user()->id);
                })
                ->where('status', true)
                ->orderBy('created_at')
                ->get();

        return response()->json([
            'status'   => 'success',
            'message'  => 'list usaha retrieved',
            'code'     => 200,
            'payload'  => fractal($usahas, new UsahaTransformer())
        ]);
    }
}
