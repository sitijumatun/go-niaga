<?php

namespace App\Http\Controllers\API;

use App\Models\Usaha;
use App\Constants\Role;
use Illuminate\Http\JsonResponse;
use App\Transformers\UsahaTransformer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UsahaController extends Controller
{
    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function listUMKM(Request $request){
        $usahas = Usaha::where('status', true)
            ->when($request->user()->role == Role::PEMILIK_USAHA || $request->user()->role == Role::OPERATOR_USAHA, static function (Builder $query) use ($request){
                return $query->where('user_id', $request->user()->id);
            })
            ->orderBy('created_at')
            ->get();

        return response()->json([
            'status'   => 'success',
            'message'  => 'list usaha retrieved',
            'code'     => 200,
            'payload'  => fractal($usahas, new UsahaTransformer())
        ]);
    }

    /**
     * @param Usaha $usaha
     * @return JsonResponse
     */
    public function detail(Usaha $usaha){
        return response()->json([
            'status'   => 'success',
            'message'  => 'list usaha retrieved',
            'code'     => 200,
            'payload'  => fractal($usaha, new UsahaTransformer())
        ]);
    }
}
