<?php

namespace App\Http\Controllers\API;

use App\Exceptions\APIException;
use App\Models\Pemasukan;
use App\Models\Pengeluaran;
use App\Models\Usaha;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\ValidationException;

class PemasukanController extends Controller
{
    /**
     * @param Request $request
     * @return JsonResponse
     * @throws APIException
     * @throws ValidationException
     */
    public function store(Request $request){
        $this->validate($request, [
            'date' => 'required',
            'usaha_id' => 'required',
            'pemasukan.*.nama_barang' => 'required',
            'pemasukan.*.pemasukan' => 'required'
        ]);

        $usaha = Usaha::find($request->get('usaha_id'));
        if(!$usaha){
            throw new APIException('UMKM tidak ditemukan!');
        }

        try{
            $resultData = array();
            foreach ($request->get('pemasukan') as $pemasukanData) {
                $input = [
                    'date' => $request->get('date'),
                    'usaha_id' => $request->get('usaha_id'),
                    'nama_barang' => $pemasukanData['nama_barang'],
                    'pemasukan' => $pemasukanData['pemasukan']
                ];

                $pemasukan = new Pemasukan();
                $pemasukan->fill($input);
                $pemasukan->usaha()->associate($usaha);
                $pemasukan->save();

                array_push($resultData, $pemasukan);
            }
        }catch (\Exception $e){
            throw new APIException($e->getMessage());
        }


        return response()->json([
            'status'   => 'success',
            'message'  => 'Input pemasukan success!',
            'code'     => 200,
            'payload'  => $resultData
        ]);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function index(Request $request){
        $dateNow = Carbon::now();
        $startToday = $dateNow->startOfDay()->format('Y-m-d H:i:s');
        $endToday = $dateNow->endOfDay()->format('Y-m-d H:i:s');

        $startWeek = $dateNow->startOfWeek()->format('Y-m-d H:i:s');
        $endWeek = $dateNow->endOfWeek()->format('Y-m-d H:i:s');

        $startMonth = $dateNow->firstOfMonth()->format('Y-m-d H:i:s');
        $endMonth = $dateNow->endOfMonth()->format('Y-m-d H:i:s');

        $pemasukan = Pemasukan::when($request->has('by'), static function ( Builder $query)  use ($request, $startToday, $endToday, $startWeek, $endWeek, $startMonth, $endMonth){
            return $query->when($request->get('by') == 'today', static function (Builder $query) use ($request, $startToday, $endToday){
                return $query->whereBetween('date', [$startToday, $endToday]);
            })->when($request->get('by') == 'week', static function (Builder $query) use ($request, $startWeek, $endWeek){
                return $query->whereBetween('date', [$startWeek, $endWeek]);
            })->when($request->get('by') == 'month', static function (Builder $query) use ($request, $startMonth, $endMonth){
                return $query->whereBetween('date', [$startMonth, $endMonth]);
            });
        })->when($request->has('date'), static function ( Builder $query)  use ($request, $startToday, $endToday, $startWeek, $endWeek, $startMonth, $endMonth){
            return $query->whereBetween('date', [Carbon::parse($request->get('date'))->startOfDay()->format('Y-m-d H:i:s'), Carbon::parse($request->get('date'))->endOfDay()->format('Y-m-d H:i:s')]);
        })
            ->sum('pemasukan');

        $pengeluaran = Pengeluaran::when($request->has('by'), static function ( Builder $query)  use ($request, $startToday, $endToday, $startWeek, $endWeek, $startMonth, $endMonth){
            return $query->when($request->get('by') == 'today', static function (Builder $query) use ($request, $startToday, $endToday){
                return $query->whereBetween('date', [$startToday, $endToday]);
            })->when($request->get('by') == 'week', static function (Builder $query) use ($request, $startWeek, $endWeek){
                return $query->whereBetween('date', [$startWeek, $endWeek]);
            })->when($request->get('by') == 'month', static function (Builder $query) use ($request, $startMonth, $endMonth){
                return $query->whereBetween('date', [$startMonth, $endMonth]);
            });
        })->when($request->has('date'), static function ( Builder $query)  use ($request, $startToday, $endToday, $startWeek, $endWeek, $startMonth, $endMonth){
            return $query->whereBetween('date', [Carbon::parse($request->get('date'))->startOfDay()->format('Y-m-d H:i:s'), Carbon::parse($request->get('date'))->endOfDay()->format('Y-m-d H:i:s')]);
        })
            ->sum('pengeluaran');

        $untung = $pemasukan - $pengeluaran;

        return response()->json([
            'status'   => 'success',
            'message'  => 'Data keuangan retrieved!',
            'code'     => 200,
            'payload'  => [
                'totalPemasukan' => $pemasukan,
                'totalPengeluaran' => $pengeluaran,
                'keuntungan' => $untung
            ]
        ]);
    }
}
