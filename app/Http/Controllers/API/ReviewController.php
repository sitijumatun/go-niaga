<?php

namespace App\Http\Controllers\API;

use App\Exceptions\APIException;
use App\Models\Review;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\ValidationException;

class ReviewController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws APIException
     */
    public function store(Request $request){
        try {
            $this->validate($request, [
                'usaha_id' => 'required',
                'review' => 'required'
            ]);
        } catch (ValidationException $e) {
            throw new APIException($e->getMessage());
        }

        $review = new Review();
        $review->fill($request->all());
        $review->usaha()->associate($request->get('usaha_id'));
        $review->user()->associate($request->user());
        $review->save();

        return response()->json([
            'status'   => 'success',
            'message'  => 'Give review success!',
            'code'     => 200,
            'payload'  => $review
        ]);
    }
}
