<?php

namespace App\Http\Controllers\API;

use App\Models\Berita;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BeritaController extends Controller
{
    /**
     * @return JsonResponse
     */
    public function index(){
        $beritas = Berita::orderBy('created_at', 'desc')->limit(5)->get();

        return response()->json([
            'status'   => 'success',
            'message'  => 'Data Retrieved!',
            'code'     => 200,
            'payload'  => $beritas
        ]);
    }
}
