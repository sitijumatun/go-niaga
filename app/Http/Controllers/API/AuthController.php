<?php

namespace App\Http\Controllers\API;

use App\Models\User;
use App\Picture;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\Facades\Image;

class AuthController extends Controller
{
    /**
     * @param Request $request
     * @return JsonResponse
     * @throws Exception
     */
    public function signup(Request $request)
    {
        $request->validate([
            'name' => 'required|string',
            'email' => 'required|string|email|unique:users',
            'password' => 'required|string|confirmed',
            'role' => 'required',
        ]);

        if($request->has('picture')) {
            $png_url = random_int(100000, 1001238912);
            $path = public_path().'/images/' . $png_url;
            Image::make(file_get_contents($request->get('picture')))->save($path);

            $user = new User([
                'name' => $request->name,
                'email' => $request->email,
                'address_type' => $request->address_type,
                'address' => $request->address,
                'role' => $request->role,
                'password' => bcrypt($request->password),
                'picture' => $png_url,
            ]);
        }else{
            $user = new User([
                'name' => $request->name,
                'email' => $request->email,
                'address_type' => $request->address_type,
                'address' => $request->address,
                'role' => $request->role,
                'password' => bcrypt($request->password)
            ]);
        }

        $user->save();

        return response()->json([
            'status'   => 'success',
            'message'  => 'signup success',
            'code'     => 200,
            'payload'  => $user
        ]);
    }


    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function login(Request $request)
    {
        $request->validate([
            'email' => 'required|string|email',
            'password' => 'required|string',
        ]);

        $credentials = request(['email', 'password']);
        if(!Auth::attempt($credentials)) {
            return response()->json([
                'message' => 'Unauthorized'
            ], 401);
        }

        $user = $request->user();
        $tokenResult = $user->createToken('Personal Access Token');
        $token = $tokenResult->token;
        if ($request->remember_me) {
            $token->expires_at = Carbon::now()->addWeeks(1);
        }
        $token->save();

        $payload = [
            'id' => $request->user()->id,
            'name' => $request->user()->name,
            'email' => $request->user()->email,
            'image_url' => $request->user()->image_url,
            'address' => $request->user()->address,
            'address_type' => $request->user()->address_type,
            'role' => $request->user()->role,
            'access_token' => $tokenResult->accessToken,
            'token_type' => 'Bearer',
            'expires_at' => Carbon::parse($tokenResult->token->expires_at)->toDateTimeString()
        ];

        return response()->json([
            'status'   => 'success',
            'message'  => 'login success',
            'code'     => 200,
            'payload'  => $payload
        ]);
    }


    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function user(Request $request)
    {
        return response()->json([
            'status'   => 'success',
            'message'  => 'User Data Retrieved!',
            'code'     => 200,
            'payload'  => $request->user()
        ]);
    }
}
