<?php


namespace App\Constants;


class AddressType extends AbstractAppConstant
{
    public const KTP = 'KTP';
    public const RUMAH = 'Rumah';
    public const PENGIRIMAN = 'Pengiriman';
}
