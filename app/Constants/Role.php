<?php


namespace App\Constants;


class Role extends AbstractAppConstant
{
    public const ROOT_SYSTEM = "Root System";
    public const OPERATOR_SYSTEM = "Operator System";
    public const PEMILIK_USAHA = "Pemilik Usaha";
    public const OPERATOR_USAHA = "Operator Usaha";
    public const INVESTOR = "Investor";
    public const OPERATOR_INVESTOR = "Operator Investor";
}
