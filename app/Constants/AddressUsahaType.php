<?php


namespace App\Constants;


class AddressUsahaType extends AbstractAppConstant
{
    public const PUSAT = 1;
    public const CABANG = 2;
}
