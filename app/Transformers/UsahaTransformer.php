<?php

namespace App\Transformers;

use App\Constants\AddressUsahaType;
use App\Models\Usaha;
use Illuminate\Database\Eloquent\Collection;
use League\Fractal\TransformerAbstract;

class UsahaTransformer extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        //
    ];
    
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        //
    ];

    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Usaha $usaha)
    {
        $usahaResult = [
            'id' => $usaha->id,
            'name' => $usaha->name,
            'category' => $usaha->category->name,
            'description' => $usaha->description,
            'pictureUrl' => $usaha->picture_url,
        ];

        $alamat = array();
        $alamatusahas = $usaha->alamatusaha()
                        ->where('status', true)
                        ->get();
        foreach ($alamatusahas as $i => $alamatusaha) {
            $alamat[$i] = [
                'addressType' => AddressUsahaType::getTitle($alamatusaha->address_type),
                'address' => $alamatusaha->address
            ];
        }

        $modalprofit = $usaha->modalprofit;
        if($usaha->modalprofit) {
            $modalprofitArray = [
                'modal'=> $modalprofit->modal ,
                'profit' => $modalprofit->profit,
                'presentaseBagiPendana' => $modalprofit->presentase_bagi_pendana,
                'jumlahHariPersiapan' => $modalprofit->jumlah_hari_persiapan,
                'periodeTransaksiPerHari' => $modalprofit->periode_transaksi_per_hari
            ];
        };

        $dataFoto = array();
        $fotos = $usaha->fotos()
            ->where('status', true)
            ->get();
        foreach ($fotos as $i => $foto) {
            $dataFoto[$i] = [
                'imageUrl' => $foto->image_url
            ];
        }

        $dataVideo = array();
        $videos = $usaha->videos()
            ->where('status', true)
            ->get();
        foreach ($videos as $i => $video) {
            $dataVideo[$i] = [
                'videoEmbeedUrl' => $video->video
            ];
        }

        $dataReview = array();
        $reviews = $usaha->review()
            ->where('status', true)
            ->get();
        foreach ($reviews as $i => $review) {
            $dataReview[$i] = [
                'review' => $review->review,
                'userGiveReview' => [
                        'name' => $review->user->name,
                        'profilePicture' => $review->user->picture_url
                ]
            ];
        }

        $usahaResult +=[
            'alamatUsaha' => count($alamat) > 0 ? $alamat : null,
            'modalProfit' => $usaha->modalprofit ? $modalprofitArray : null,
            'galeriVideo' => [
                'foto' => count($dataFoto) > 0 ? $dataFoto : null,
                'video' => count($dataVideo) > 0 ? $dataVideo : null
            ],
            'Review' => count($dataReview) > 0 ? $dataReview : null
        ];

        return $usahaResult;
    }
}
