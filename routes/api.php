<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::group(['prefix' => 'auth', 'namespace'=> 'API'], function () {
    Route::post('login', 'AuthController@login');
    Route::post('signup', 'AuthController@signup');
});

Route::group(['namespace'=> 'API'], function () {
    Route::get('role', 'RoleController@index');
});

Route::group(['middleware' => 'auth:api', 'namespace' => 'API'], function() {
    Route::get('logout', 'AuthController@logout');
    Route::get('user', 'AuthController@user');

    Route::get('category', 'CategoryController@index');
    Route::get('category/usaha/{category}', 'CategoryController@listUMKM');

    Route::get('usaha','UsahaController@listUMKM');
    Route::get('usaha/detail/{usaha}','UsahaController@detail');

    Route::post('review', 'ReviewController@store');
    Route::get('berita', 'BeritaController@index');

    Route::post('pemasukan/input', 'PemasukanController@store');
    Route::post('pengeluaran/input', 'PengeluaranController@store');

    Route::get('keuangan', 'PemasukanController@index');
});
