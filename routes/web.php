<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/login', function () {
    return view('auth.login');
});

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');

Route::group(['middleware' => 'auth','namespace' => 'Web'], static function() {
    Route::get('dashboard', 'DashboardController@index')->name('dashboard.index');

    Route::resource('category', 'CategoryController');
    Route::get('category/delete/{category}', 'CategoryController@destroy')->name('category.delete');
    Route::get('category/update/status/{status}/{category}', 'CategoryController@updateStatus')->name('category.update.status');

    Route::resource('usaha', 'UsahaController');
    Route::get('usaha/delete/{usaha}','UsahaController@destroy')->name('usaha.delete');
    Route::get('usaha/update/status/{status}/{usaha}','UsahaController@updateStatus')->name('usaha.update.status');

    Route::resource('alamatusaha', 'AlamatUsahaController');
    Route::get('alamatusaha/create/{usaha}','AlamatUsahaController@create')->name('alamatusaha.create');
    Route::get('alamatusaha/delete/{alamatusaha}','AlamatUsahaController@destroy')->name('alamatusaha.delete');
    Route::get('alamatusaha/update/status/{status}/{alamatusaha}','AlamatUsahaController@updateStatus')->name('alamatusaha.update.status');

    Route::resource('modalprofit','ModalProfitController');
    Route::get('modalprofit/create/{usaha}','ModalProfitController@create')->name('modalprofit.create');

    Route::resource('foto', 'FotoController');
    Route::get('foto/create/{usaha}','FotoController@create')->name('foto.create');
    Route::get('foto/delete/{foto}','FotoController@destroy')->name('foto.delete');
    Route::get('foto/update/status/{status}/{foto}','FotoController@updateStatus')->name('foto.update.status');

    Route::resource('video', 'VideoController');
    Route::get('video/create/{usaha}','VideoController@create')->name('video.create');
    Route::get('video/delete/{video}','VideoController@destroy')->name('video.delete');
    Route::get('video/update/status/{status}/{video}','VideoController@updateStatus')->name('video.update.status');

    Route::get('review/update/status/{status}/{review}','ReviewController@updateStatus')->name('review.update.status');

    Route::resource('berita', 'BeritaController');
    Route::get('berita/delete/{berita}', 'BeritaController@destroy')->name('berita.delete');
    Route::get('berita/update/status/{status}/{berita}', 'BeritaController@updateStatus')->name('berita.update.status');

    Route::resource('user', 'UserController');
    Route::get('user/delete/{user}', 'UserController@destroy')->name('user.delete');
    Route::get('user/update/status/{status}/{user}', 'UserController@updateStatus')->name('user.update.status');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
