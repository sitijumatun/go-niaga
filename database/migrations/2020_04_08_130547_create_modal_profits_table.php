<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModalProfitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('modal_profits', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('modal');
            $table->integer('profit');
            $table->integer('usaha_id')->unsigned();
            $table->integer('presentase_bagi_pendana');
            $table->integer('jumlah_hari_persiapan');
            $table->integer('periode_transaksi_per_hari');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('modal_profits');
    }
}
